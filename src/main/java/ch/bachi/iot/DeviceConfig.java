package ch.bachi.iot;

import com.typesafe.config.ConfigObject;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;

public class DeviceConfig {

    public String token;
    public String app;
    public String email;
    public final HashMap<String, String> webhooks = new HashMap<>();
    public Duration alertIfNoSignalFor;

    // Timestamp of the last signal/request from
    public LocalDateTime lastSignalReceived;

    public DeviceConfig(com.typesafe.config.Config cfg) {
        token = cfg.getString("token");
        app = cfg.getString("app");
        email = cfg.getString("email");

        if (cfg.hasPath("alertIfNoSignalFor")) {
            alertIfNoSignalFor = cfg.getDuration("alertIfNoSignalFor");
        }

        if (cfg.hasPath("webhooks")) {
            for (ConfigObject hookcfg : cfg.getObjectList("webhooks")) {
                hookcfg.entrySet().stream().findFirst().ifPresent(hook -> {
                    webhooks.put(hook.getKey(), (String) hook.getValue().unwrapped());
                });
            }
        }
    }
}

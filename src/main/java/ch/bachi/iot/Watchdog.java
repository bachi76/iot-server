package ch.bachi.iot;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.UnirestException;
import org.slf4j.LoggerFactory;

import java.lang.management.ManagementFactory;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * The Watchdog monitors all configured apps for received device signals and configured timeouts.
 * If no signal is received within the defined period, a message to the alert webhook (if configured)
 * of that device is sent.
 */
class Watchdog {
    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(Watchdog.class);

    // Run watchdog ever n seconds
    private static final int WATCH_PERIOD = 10;
    private static final Set<Integer> fiboThrottler = new HashSet<>(Arrays.asList(
            1, 21, 89, 144, 233, 377, 610, 987, 1597, 2584
    ));
    private HashMap<String, Integer> eventCount = new HashMap<>();

    private ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

    Watchdog(Config config) {

        executor.scheduleWithFixedDelay(() -> {

            for (DeviceConfig entry : config.devices.values()) {

                try {

                    // Is the signal overdue (and an alert configured)?
                    if (entry.alertIfNoSignalFor != null && !entry.alertIfNoSignalFor.isZero()
                            && ManagementFactory.getRuntimeMXBean().getUptime() > entry.alertIfNoSignalFor.toMillis()
                            && (entry.lastSignalReceived == null || entry.lastSignalReceived.isBefore(LocalDateTime.now().minus(entry.alertIfNoSignalFor)))) {

                        // Calculate the watchdog event count (= time since last signal divided by alertIfNoSignalFor)
                        long secSinceLastSignal;
                        if (entry.lastSignalReceived != null) {
                            secSinceLastSignal = Duration.between(entry.lastSignalReceived, LocalDateTime.now()).getSeconds();
                        } else {
                            secSinceLastSignal = ManagementFactory.getRuntimeMXBean().getUptime() / 1000;
                        }
                        long numEvents = secSinceLastSignal / entry.alertIfNoSignalFor.getSeconds();

                        if (eventCount.getOrDefault(entry.token, 0) == numEvents) {
                            // been here before.
                            continue;
                        }
                        eventCount.put(entry.token, (int) numEvents);

                        // Create the payload (for the webhook as well as Elastic logging)
                        String payload = String.format("{'severity':'alert', 'tag':'Watchdog', 'msg':'No signal from %s received since %s', 'created':'%s'}",
                                entry.app,
                                (entry.lastSignalReceived != null) ? entry.lastSignalReceived.toString() : "server start",
                                LocalDateTime.now().toString()).
                                replace("'", "\"");

                        // Throttle alert messages with Fibonacci's help..
                        if (fiboThrottler.contains((int) numEvents)) {

                            // Send the Watchdog error via the normal IoT REST interface.
                            // (it will detect the sender and not count this as device signal)
                            try {
                                String url = String.format("http://localhost:%d/api/db/log/", config.port);
                                HttpResponse<JsonNode> jsonResponse = Unirest
                                        .post(url)
                                        .header("Content-Type", "application/json")
                                        .header("token", entry.token)
                                        .body(payload)
                                        .asJson(); // server must respond with json

                                logger.warn("'No signal' alert for app '{}', sending to {} at localhost with payload '{}'. Result: {}",
                                        entry.app, url, payload, jsonResponse.getStatus());

                            } catch (UnirestException e) {
                                e.printStackTrace();
                            }

                        } else {
                            logger.debug("Holding back 'no signal' #{} alert for app '{}' due to throttling.", numEvents, entry.app);
                        }


                    } else {
                        eventCount.put(entry.token, 0);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }, 1, WATCH_PERIOD, TimeUnit.SECONDS);


    }

    void stop() {
        executor.shutdown();
    }

}

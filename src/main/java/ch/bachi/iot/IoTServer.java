package ch.bachi.iot;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.UnirestException;
import org.apache.http.util.TextUtils;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Spark;

import java.net.UnknownHostException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import org.apache.commons.lang3.StringUtils;

//import kong.unirest.http.exceptions.UnirestException;


public class IoTServer {
	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(IoTServer.class);

	private Config config = new Config();
	private Watchdog watchdog;

	public static void main(String[] args) throws InterruptedException {

		IoTServer server = new IoTServer();
		try {
			server.init();

			Runtime.getRuntime().addShutdownHook(new Thread(server::shutdown));

			while (true) {
				Thread.sleep(200);
			}


		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	private void shutdown() {
		logger.info("Shutting down...");
		Spark.stop();
		watchdog.stop();
	}

	private void init() throws UnknownHostException {

//		Settings settings = Settings.builder()
//				.put("cluster.name", "elasticsearch").build();
//		elastic = new PreBuiltTransportClient(settings)
//				.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(config.elasticHost), config.elasticPort));

		// Mount the static files folder (default: ./static/)
		if (!TextUtils.isEmpty(config.staticFilesFolder)) {
			Spark.staticFiles.externalLocation(config.staticFilesFolder);
		}

		Spark.port(config.port);
		Spark.get("/", (req, res) -> String.format("Welcome to the iot-server.<br>%d devices configured.", config.devices.size()));


		// All /api/* routes require a valid token & update last signal received
		Spark.before("/api/*", (request, response) -> {
			String token = request.headers("token");
			if (!config.devices.containsKey(token)) {
				throw Spark.halt(401, "No valid token.");
			}

			// Count only signals of remote device (not watchdog events)
			if (!request.ip().equals("127.0.0.1")) {
				config.devices.get(token).lastSignalReceived = LocalDateTime.now();
			}

		});


		// READ from Elasic
		Spark.get("/api/db/:type/:id", ((request, response) -> {
			String token = request.headers("token");
			String app = config.devices.get(token).app;

			String url = String.format("%s/%s/%s/%s",
					config.elasticUrl,
					app,
					request.params(":type"),
					request.params(":id"));


			String result;
			try {
				HttpResponse<JsonNode> jsonResponse = Unirest
                        .get(url)
                        .asJson();

				int status = jsonResponse.getStatus();
				response.status(status);
				if (status / 100 == 2) {
					result = jsonResponse.getBody().getObject().getJSONObject("_source").toString();
					response.type("application/json");

				} else {
					result = jsonError(String.valueOf(status));
				}

			} catch (UnirestException e) {
				logger.error("Error: " + e.getMessage());
				result = jsonError(e.getMessage());
				response.status(500);
			}

			logger.debug("GET request from app '{}' ({}). Sending to {}, Result: '{}'",
					app, request.ip(), url, result);

			return result;
		}));

		// WRITE to Elastic
		Spark.post("/api/db/:type/", ((request, response) -> {
			String token = request.headers("token");
			String app = config.devices.get(token).app;

			String url = String.format("%s/%s-%s/_doc",
					config.elasticUrl,
					app,
					request.params(":type"));

			String payload = processPayload(request);

			// POST to Elastic
			String result;
			try {
				HttpResponse<JsonNode> jsonResponse = Unirest
                        .post(url)
						.header("Content-Type", "application/json")
                        .body(payload)
                        .asJson();

				int status = jsonResponse.getStatus();
				result = (status / 100 == 2) ? jsonResponse.getBody().getObject().optString("_id") : jsonError(String.valueOf(status));
				response.status(status);

			} catch (UnirestException e) {
				logger.error(e.getMessage());
				result = jsonError(e.getMessage());
				response.status(500);
			}

			logger.debug("POST request from app '{}' ({}) with payload '{}'. Sending to {}, Result: '{}'",
					app, request.ip(), payload, url, result);

			// Trigger matching webhooks
			// Note: The API response reflects the Elastic operation's result, not the Webhook's (i.e. a failed webhook is not noticed on the calling endpoint)
			String key = request.params(":type");
			String webhookUrl = config.devices.get(token).webhooks.getOrDefault(key, null);

			// If it's a log entry and there's no overall 'log' webhook, also try the specific webhook key (e.g. "log.error")
			if (key.equals("log") && webhookUrl == null) {
				JSONObject jsonObject = new JsonNode(payload).getObject();
				if (jsonObject.has("severity")) {
					String severity = jsonObject.getString("severity");
					webhookUrl = config.devices.get(token).webhooks.getOrDefault(key + "." + severity, null);
				}
			}

			String resultW;
			if (webhookUrl != null) {
				HttpResponse<JsonNode> jsonResponseW = null;
				try {
					jsonResponseW = Unirest
                            .post(webhookUrl)
							.header("Content-Type", "application/json")
                            .body(payload)
                            .asJson();

					// FIXME : Status not really set..
					resultW = (jsonResponseW.getStatus() / 100 == 2) ? jsonResponseW.getBody().getObject().optString("_id") : jsonError(String.valueOf(jsonResponseW.getStatus()));
					response.status(jsonResponseW.getStatus());

				} catch (UnirestException e) {
					logger.error(e.getMessage());
					resultW = jsonError(e.getMessage());
				}

				logger.debug("POST request from app '{}' ({}) with payload '{}'. Sending to webhook {}, Result: '{}'",
						app, request.ip(), payload, webhookUrl, resultW);
			}

			return result;
		}));

		// UPDATE in Elastic
		Spark.put("/api/db/:type/:id", ((request, response) -> {
			String token = request.headers("token");
			String app = config.devices.get(token).app;

			String url = String.format("%s/%s-%s/%s",
					config.elasticUrl,
					app,
					request.params(":type"),
					request.params(":id"));

			String payload = processPayload(request);

			String result;
			try {
				HttpResponse<JsonNode> jsonResponse = Unirest
                        .put(url)
						.header("Content-Type", "application/json")
                        .body(payload)
                        .asJson();

				int status = jsonResponse.getStatus();
				response.status(status);
				result = (status / 100 == 2) ? jsonResponse.getBody().getObject().optString("result") : jsonError(String.valueOf(status));

			} catch (UnirestException e) {
				logger.error(e.getMessage());
				result = jsonError(e.getMessage());
			}

			logger.debug("PUT request from app '{}' ({}) with payload '{}'. Sending to {}, Result: '{}'",
					app, request.ip(), payload, url, result);

			return result;
		}));


		// Enable the watchdog service to alert if devoices are offline
		watchdog = new Watchdog(config);
	}

	private String jsonError(String errorMsg) {
		return String.format("{\"error\": \"%s\"}", errorMsg);
	}

	/**
	 * Add a creation date (if not present) and the ip address to each document
	 */
	private String processPayload(Request request) {
		JSONObject jsonObject = new JsonNode(request.body()).getObject();
		if (!jsonObject.has("created")) {
			// Need to remove the [Europe/Zurich] part for Elastic
			String created = ZonedDateTime.now().toString();
			created = StringUtils.substringBefore(created, "[");
			jsonObject.put("created" , created);
		}
		jsonObject.put("ip" , request.ip());
		return jsonObject.toString();
	}
}

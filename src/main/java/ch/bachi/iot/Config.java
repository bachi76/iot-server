package ch.bachi.iot;

import com.typesafe.config.ConfigFactory;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.HashMap;

/**
 * Config
 * Reads in a ./iot-server.conf file and uses the supplied application.conf as fallback/default.
 */
public class Config {
	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(Config.class);

	private com.typesafe.config.Config conf;

	public int port;
	public String elasticUrl;
	public String staticFilesFolder;
	public HashMap<String, DeviceConfig> devices = new HashMap<>();

	public Config() {

		String cfgPathDev = "./devops/iot-server.conf";
		String cfgPathProd = "./config/iot-server.conf";

		// Try to load dev version in /devops first. On production, that folder is not present,
		// so load from prod path there.
		File configFile = new File(cfgPathDev);
		if (configFile.isFile()) {
			logger.info("Loading config from " + cfgPathDev);

		} else {

			logger.info("Loading config from " + cfgPathProd);
			configFile = new File(cfgPathProd);
			if (!configFile.isFile()) {
				logger.error("Config file '{}' not found.", cfgPathProd);
				throw new RuntimeException("Config file '" + cfgPathProd + "' not found.");
			}
			if (!configFile.canRead()) {
				logger.error("Cannot read file '{}'.", cfgPathProd);
				throw new RuntimeException("Cannot read '" + cfgPathProd + "'");
			}
		}

		conf = ConfigFactory.parseFile(configFile);

		port = conf.getInt("port");
		elasticUrl = conf.getString("elasticUrl");
		staticFilesFolder = conf.getString("staticFilesFolder");

		for (com.typesafe.config.Config devcfg : conf.getConfigList("devices")) {
			devices.put(devcfg.getString("token"), new DeviceConfig(devcfg));
		}
	}

}

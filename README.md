IoT Server
==========

***Looking for an easy to setup one-stop solution for storing, analyzing, visualizing and sharing your internet of things data? Try the IoT Server.***
#
![Example of my ESP8266 based PlantWizard dashboard](/images/kibana-dashboard.png)

Features
--------

The IoT server is a tiny REST API layer over the well-known ELK stack:
 
 * Elastic Search as storage engine
 * Kibana as data analyzing and visualisation tool
 * nginx proxy for access and tsl management

It also offers a simple webserver for static web app hosting (see [iot-webapp-template](https://bitbucket.org/bachi76/iot-app-template))

All in a neat Docker container for painless setup and management.

Prerequisites
-------------
You (obviously) need a Docker host. Either [install it](https://docs.docker.com/engine/installation/) on your host, or 
use a cloud service that offers docker hosting. If you're new to docker, I suggest to take some time to learn the basics.


Installation
------------

**1. Configure your client app(s):**

Rename `<project-root>/devops/iot-server.conf.dist` to `iot-server.conf` and add an entry for your client app(s):

```
devices = [
  {
    app: helloworld
    token: 6zJpwyN6MCuDnQvDQ7ee
    email: "your@email.com"
  }
  {
      app: another-app
      token: someothertsecrettoken
      email: "your@email.com"
  }
]
```
You can also configure webhooks to forward specific message types to. Matching POSTs will be forwarded to the webhook
url. The post body contains json and the server response is also expected to be json. This works well
with services like zapier.com.

If you set `alertIfNoSignalFor`,  the iot server will trigger alerts if there's no incoming data from this device.

Example:

```
devices = [
  {
    app: helloworld
    token: 6zJpwyN6MCuDnQvDQ7ee
    email: "your@email.com"

    # You can forward POSTs from your device to any webhook url, such as Zapier webhooks. The key must match the key in the POST message's json body.
    # Key "log" will forward all log entries sent using Log::i() etc. on the device
    # Key "alert" is meant for alerts that require immediate attention. You can also choose if Log::e() should be sent to the alert webhook.
    webhooks = [
      {log: "https://your-webhook-url-for-all-log-entries"}
      {temperature: "https://your-webhook-url-for-posts-with-the-key-'temperature'"}
      {alert: "https://your-webhook-url-for-alerts"}
    ]

    # Issues an alert webhook push if there's no incoming request from this device for a given time period.
    alertIfNoSignalFor: 30m
  }
]
```

## ##
**2. Build the image, create and run a container:**

```
cd <project-root>/devops
docker build -t bachi/iot-server .
docker run -d --name iot-server -v <host folder for elastic data>:/usr/share/elasticsearch/data -v <host folder for static web apps>:/www -p 80:80 -p 443:443 -p 4567:4567 -e SSL=true -e ELSK_USER="<user>" -e ELSK_PASS="<pw>" bachi/iot-server

```
Check if all services came up correctly:
```docker logs --follow <container-id>```

Notes:

* `ELK_USER` and `ELK_PASS` are used by nginx to protect web access to Kibana
* Port 80 and 443 are used for access to Kibana, port 4567 exposes the REST API and static web files
* If your host has a webserver already, either use a different port than 80/443 for Kibana, or add a virtual host that
  acts as reverse proxy. 
* The volume mapping will persist Elastic data. You might want to ensure the host folder is backed up.
* Services are managed by supervisord (see supervisor-iot.conf for details)
* To follow the iot-server log: ```docker exec -it iot-server tail -f /var/log/iot-server.stdout.log```

## ##
**3. Insert data and configure Kibana**:

Send some POST requests to add some data (see example below), then open Kibana (on your docker host and mapped port). It asks for an 
index to be created, replace `logstash-*` with your configured app name (or use *), and select `created` as the timestamp field. Then go to "Discover" to find your data. It should look like this:

![Kibana after the first example record was POSTed](/images/kibana-hello-world.png)

#

REST API
--------
All endpoints require a valid app token in the `token` header field.

The `type` defines the data structure (fields within a type should not change). Think of a table in classical DBMS.

Every app uses its own ElasticSearch index (think database), defined by the app name matching the passed app token.

| Endpoint                  | Desc |
| --------                  | ---- |
| **GET /api/db/:type/:id**     | Get the record with the given id and type |
| **POST /api/db/:type/**       | Store a new record. Put the JSON payload in the request body. Elastic automatically assigns a unique id. |
| **UPDATE /api/db/:type/:id**  | Update the existing record with the given id|
| *DELETE ...*                | *(todo)* |

### Exampe request: ###

```
POST /api/db/test/ HTTP/1.1
Host: localhost:4567
token: 6zJpwyN6MCuDnQvDQ7ee
Content-Type: application/json

{
	"msg": "Hello world!"
}
```

The server replies with a "201 Created" code and the elastic id of the new record in the body.


ESP8266 Arduino - client library
----------------------
Install and add the [ESP8266-Framework](https://bitbucket.org/bachi76/esp8266-framework) library to your project. Use it like this:

```c
IotServer iot("<iot api url>", "<app token>");
iot.post("free_heap", (int)ESP.getFreeHeap());
```

To use remote logging:

```c
Log::enableRemoteLogging(iot);
Log::w(TAG, "Something happened!");

```

For more details and examples of more complex documents, see the project docs.


Security
--------

 * Web access to the Kibana frontend is secured by HTTPS (self-signed cert) + Basic Auth by default. For more options, 
especially how to add a trusted cert, see [here]()
 * Access to Elastic Search (port 9200 / 9300) is not exposed outside the container (unless you map them of course)
 * The REST api on port 4567 requires a valid app token. However, it is (currently) HTTP only. This is a security concern, 
  for many iot devices (such as the ESP8266), HTTPS is out of scope due to limited resources. If you prefer HTTPS here,
  you could configure nginx to also ssl proxy :4567 as it does for Kibana. 


Host static web apps
--------------------
- Upload web files to the host's volume mapped to the docker volume.
- Access them on `http://<your-host>:4567/`
- There's a neat [iot webapp template](https://bitbucket.org/bachi76/iot-app-template) (based on Vue.js/Vuetify/Material design)


Tips and Tricks
-------------

 * After the first installation, Kibana asks for index creation. Obviously, you need first some data - send some post requests!
 * The iot server adds the origin `ip` and the `created` timestamp fields to each post request. In Kibana's advanced settings, 
   set `timelion:es.timefield` to `created` _(TODO: pre-configure or use the default @timestamp)_
 * Log into your docker container using `docker exec -it /bin/bash`
 * Edit the client app config: Change local.conf, then re-deploy the image or log into the container and modify local.conf there, then restart (TODO: Move all configs to the mapped volume) 


More information
----------------
The ELK stack is built using the [Blacktop image](https://github.com/blacktop/docker-elastic-stack) - check out the great 
docs for more info on setup, nginx config etc.

Learn more about [ElasticSearch](https://www.elastic.co/products/elasticsearch) and [Kibana](https://www.elastic.co/products/kibana)


Contribute
----------
Need a certain plugin or feature? Found a bug? Reports welcome - pull requests even more so. 


Legal note
----------
Copyright 2017 Martin Bachmann, insign gmbh, www.insign.ch

Licensed under the *Apache License, Version 2.0* (the "License");
you may not use this library except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.